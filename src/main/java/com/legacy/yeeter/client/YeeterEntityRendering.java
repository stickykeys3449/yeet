package com.legacy.yeeter.client;

import com.legacy.yeeter.entity.TNTYeeterEntity;

import net.minecraft.entity.Entity;
import net.minecraftforge.fml.client.registry.IRenderFactory;
import net.minecraftforge.fml.client.registry.RenderingRegistry;

public class YeeterEntityRendering
{
	public static void init()
	{
		register(TNTYeeterEntity.class, YeeterRenderer::new);
	}

	private static <T extends Entity> void register(Class<T> entityClass, IRenderFactory<? super T> renderFactory)
	{
		RenderingRegistry.registerEntityRenderingHandler(entityClass, renderFactory);
	}
}