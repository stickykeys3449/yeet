package com.legacy.yeeter.client;

import com.legacy.yeeter.entity.TNTYeeterEntity;

import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;

public class YeeterModel<T extends Entity> extends EntityModel<T>
{

	public RendererModel head;

	public RendererModel stonecutter_top;

	public RendererModel right_arm2;

	public RendererModel right_arm1;

	public RendererModel stonecutter_bottom;

	public RendererModel left_arm1;

	public RendererModel left_arm2;

	public RendererModel left_leg1;

	public RendererModel right_leg1;

	public RendererModel right_leg2;

	public RendererModel left_leg2;

	public RendererModel tnt;

	public YeeterModel()
	{
		this.textureWidth = 128;
        this.textureHeight = 64;
		this.head = new RendererModel(this, 0, 0);
		this.head.setRotationPoint(0.0F, 0.0F, -6.0F);
		this.head.addBox(-4.0F, -4.0F, -8.0F, 8, 8, 8, 0.0F);
		this.stonecutter_bottom = new RendererModel(this, 0, 16);
		this.stonecutter_bottom.setRotationPoint(0.0F, 8.0F, 0.0F);
		this.stonecutter_bottom.addBox(-4.0F, 0.0F, -6.0F, 8, 12, 12, 0.0F);
		this.left_leg1 = new RendererModel(this, 0, 40);
		this.left_leg1.mirror = true;
		this.left_leg1.setRotationPoint(4.0F, 14.0F, -0.5F);
		this.left_leg1.addBox(0.0F, -3.0F, -2.5F, 2, 6, 6, 0.0F);
		this.stonecutter_top = new RendererModel(this, 0, 16);
		this.stonecutter_top.setRotationPoint(0.0F, 8.0F, 0.0F);
		this.stonecutter_top.addBox(-4.0F, -12.0F, -6.0F, 8, 12, 12, 0.0F);
		this.right_leg1 = new RendererModel(this, 0, 40);
		this.right_leg1.mirror = true;
		this.right_leg1.setRotationPoint(-4.0F, 14.0F, -0.5F);
		this.right_leg1.addBox(-2.0F, -3.0F, -2.5F, 2, 6, 6, 0.0F);
		this.right_arm2 = new RendererModel(this, 28, 17);
		this.right_arm2.setRotationPoint(-4.0F, 1.5F, -0.5F);
		this.right_arm2.addBox(-2.0F, -10.0F, -1.5F, 2, 7, 4, 0.0F);
		this.right_arm1 = new RendererModel(this, 0, 40);
		this.right_arm1.setRotationPoint(-4.0F, 1.5F, -0.5F);
		this.right_arm1.addBox(-2.0F, -3.0F, -2.5F, 2, 6, 6, 0.0F);
		this.left_leg2 = new RendererModel(this, 28, 17);
		this.left_leg2.mirror = true;
		this.left_leg2.setRotationPoint(4.0F, 14.0F, -0.5F);
		this.left_leg2.addBox(0.0F, 3.0F, -1.5F, 2, 7, 4, 0.0F);
		this.left_arm1 = new RendererModel(this, 0, 40);
		this.left_arm1.setRotationPoint(4.0F, 1.5F, -0.5F);
		this.left_arm1.addBox(0.0F, -3.0F, -2.5F, 2, 6, 6, 0.0F);
		this.left_arm2 = new RendererModel(this, 28, 17);
		this.left_arm2.mirror = true;
		this.left_arm2.setRotationPoint(4.0F, 1.5F, -0.5F);
		this.left_arm2.addBox(0.0F, -10.0F, -1.5F, 2, 7, 4, 0.0F);
		this.right_leg2 = new RendererModel(this, 28, 17);
		this.right_leg2.setRotationPoint(-4.0F, 14.0F, -0.5F);
		this.right_leg2.addBox(-2.0F, 3.0F, -1.5F, 2, 7, 4, 0.0F);
		this.tnt = new RendererModel(this, 40, 0);
        this.tnt.setRotationPoint(0.0F, -8.5F, 0.0F);
        this.tnt.addBox(-8.0F, -16.0F, -8.0F, 16, 16, 16, 0.0F);
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		this.head.render(f5);
		this.stonecutter_bottom.render(f5);
		this.left_leg1.render(f5);
		this.stonecutter_top.render(f5);
		this.right_leg1.render(f5);
		this.right_arm2.render(f5);
		this.right_arm1.render(f5);
		this.left_leg2.render(f5);
		this.left_arm1.render(f5);
		this.left_arm2.render(f5);
		this.right_leg2.render(f5);
		
		if (((TNTYeeterEntity)entity).getTNTShown())
		{
			this.tnt.render(f5);
		}
	}

	public void setRotateAngle(RendererModel modelRenderer, float x, float y, float z)
	{
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
	
	@Override
	public void setRotationAngles(T entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scaleFactor)
    {
		super.setRotationAngles(entityIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scaleFactor);
		
		this.head.rotateAngleX = headPitch * 0.017453292F;
        this.head.rotateAngleY = netHeadYaw * 0.017453292F;
        
        this.stonecutter_top.rotateAngleX = 0;
        this.stonecutter_bottom.rotateAngleX = 0;

        this.right_leg1.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
        this.left_leg1.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float)Math.PI) * 1.4F * limbSwingAmount;
        
        this.right_leg2.rotateAngleX = this.right_leg1.rotateAngleX;
        this.left_leg2.rotateAngleX = this.left_leg1.rotateAngleX;
        
        this.left_arm1.rotateAngleX = !((TNTYeeterEntity)entityIn).getArmsRaised() ? MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount + 3.15F : 0.0F;
        this.right_arm1.rotateAngleX = !((TNTYeeterEntity)entityIn).getArmsRaised() ? MathHelper.cos(limbSwing * 0.6662F + (float)Math.PI) * 1.4F * limbSwingAmount + 3.15F : 0.0F;
        
        this.right_arm2.rotateAngleX = this.right_arm1.rotateAngleX;
        this.left_arm2.rotateAngleX = this.left_arm1.rotateAngleX;
    }
}
