package com.legacy.yeeter;

import com.legacy.yeeter.entity.TNTYeeterEntity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraft.util.registry.Registry;
import net.minecraftforge.registries.ObjectHolder;

@ObjectHolder("yeeter")
public class YeeterEntityTypes
{
	public static final EntityType<TNTYeeterEntity> TNT_YEETER = register("tnt_yeeter", EntityType.Builder.create(TNTYeeterEntity::new, EntityClassification.MONSTER).size(0.6F, 1.99F));

	@SuppressWarnings("deprecation")
	private static <T extends Entity> EntityType<T> register(String id, EntityType.Builder<T> builder)
	{
		EntityType<T> entity = Registry.register(Registry.ENTITY_TYPE, "yeeter:" + id, builder.build(id));
		return entity;
	}
}
